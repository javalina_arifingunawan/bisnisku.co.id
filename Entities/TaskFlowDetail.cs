﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class TaskFlowDetail
    {
        public Guid TaskFlowDetailId { get; set; }
        public Guid TaskFlowId { get; set; }
        public Guid TaskId { get; set; }
        public Guid? ParentTaskId { get; set; }
        public string TaskDescription { get; set; }
        public string TaskFlowDetailValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
