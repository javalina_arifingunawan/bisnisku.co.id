﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface ITaskFlow
    {
        IEnumerable<Entities.TaskFlow> GetAll();
    }
    public class TaskFlow : ITaskFlow
    {
        public IEnumerable<Entities.TaskFlow> GetAll()
        {
            //var result = new List<Entities.TaskFlow>();
            //result.Add(new Entities.TaskFlow{ TaskFlowId = Guid.NewGuid(), TaskName = "Pembuatan PT", CreatedDate = DateTime.Now, IsDeleted = false });
            //result.Add(new Entities.TaskFlow { TaskFlowId = Guid.NewGuid(), TaskName = "Pembuatan CV", CreatedDate = DateTime.Now, IsDeleted = false });
            //result.Add(new Entities.TaskFlow { TaskFlowId = Guid.NewGuid(), TaskName = "Pembuatan UD", CreatedDate = DateTime.Now, IsDeleted = false });

            const string sql = @"
                SELECT [TaskFlow].[TaskFlowId]
                      ,[TaskFlow].[TaskName]
                      ,[TaskFlow].[CreatedDate]
                      ,[TaskFlow].[IsDeleted]
                  FROM [dbo].[TaskFlow]
            ";
            var result = Dapper.QueryMain<Entities.TaskFlow>(sql);

            return result;
        }
    }
}
