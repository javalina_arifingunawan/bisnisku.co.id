﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Repositories
{
    public static class Dapper
    {
        private static bool _isInitialized;
        private static string _mainConnectionString;
        private static readonly Random _random = new Random();

        public static void Initialize(string mainConnectionString)
        {
            try
            {
                _mainConnectionString = mainConnectionString;
                _isInitialized = true;
            }
            catch (Exception x)
            {
                _isInitialized = false;
                throw x;
            }
        }


        #region Query

        internal static IEnumerable<T> QueryMain<T>(string sql, dynamic param = null, System.Data.IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, System.Data.CommandType? commandType = null)
        {
            try
            {
                CheckInitialization();

                return Query<T>(_mainConnectionString, sql, param, transaction, buffered, commandTimeout, commandType);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + FormatException(sql, param), ex);
            }
        }

        internal static IEnumerable<T> QueryMainXML<T>(string sql, dynamic param = null, System.Data.IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, System.Data.CommandType? commandType = null)
        {
            try
            {
                CheckInitialization();

                string xml = string.Join(string.Empty, ((IEnumerable<string>)Query<string>(_mainConnectionString, sql, param, transaction, buffered, commandTimeout, commandType)));

                if (string.IsNullOrWhiteSpace(xml))
                {
                    return Enumerable.Empty<T>();
                }

                using (var reader = new StringReader(xml))
                {
                    var ser = new XmlSerializer(typeof(List<T>));
                    return (List<T>)ser.Deserialize(reader);
                }
            }
            catch (Exception x)
            {
                throw new Exception(x.Message + FormatException(sql, param), x);
            }
        }

        #endregion

        #region Execute

        internal static int ExecuteMain(string sql, dynamic param = null, System.Data.IDbTransaction transaction = null, int? commandTimeout = null, System.Data.CommandType? commandType = null)
        {
            try
            {
                CheckInitialization();

                return Execute(_mainConnectionString, sql, param, transaction, commandTimeout, commandType);
            }
            catch (Exception x)
            {
                throw new Exception(x.Message + FormatException(sql, param), x);
            }
        }

        #endregion

        #region Private Methods

        private static void CheckInitialization()
        {
            if (!_isInitialized)
            {
                throw new Exception("Initialize has not been called");
            }
        }

        private static IEnumerable<T> Query<T>(string connectionString, string sql, dynamic param = null, System.Data.IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, System.Data.CommandType? commandType = null)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    return SqlMapper.Query<T>(connection, sql, param, transaction, buffered, commandTimeout, commandType);
                }
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        private static int Execute(string connectionString, string sql, dynamic param = null, System.Data.IDbTransaction transaction = null, int? commandTimeout = null, System.Data.CommandType? commandType = null)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    return SqlMapper.Execute(connection, sql, param, transaction, commandTimeout, commandType);
                }
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        private static string FormatException(string sql, dynamic param)
        {
            return "\r\nparam: " + JsonConvert.SerializeObject(param) + "\r\nsql: " + sql;
        }

        #endregion

    }
}
