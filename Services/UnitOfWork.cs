﻿using System;
using System.Transactions;

namespace Services
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private TransactionScope _scope;

        public UnitOfWork()
        {
            var transactionOptions = new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(120)
            };

            _scope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
        }

        public void Commit()
        {
            if (_scope == null)
            {
                throw new InvalidOperationException("The unit of work cannot be committed because it has already been committed or the unit of work has been disposed.");
            }

            _scope.Complete();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_scope != null)
                {
                    _scope.Dispose();
                    _scope = null;
                }
            }
        }
    }
}
