﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface ITaskFlow
    {
        IEnumerable<Entities.TaskFlow> GetAll();
    }
    public class TaskFlow : ITaskFlow
    {
        private readonly Repositories.ITaskFlow _taskFlowRepository;

        public TaskFlow(Repositories.ITaskFlow taskFlowRepository)
        {
            _taskFlowRepository = taskFlowRepository;
        }

        public IEnumerable<Entities.TaskFlow> GetAll()
        {
            try
            {
                return _taskFlowRepository.GetAll();
            } catch(Exception e)
            {
                throw e;
            }
        }
    }
}
