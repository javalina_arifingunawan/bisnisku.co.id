﻿namespace Services
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }

    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public UnitOfWorkFactory()
        {
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork();
        }
    }
}
