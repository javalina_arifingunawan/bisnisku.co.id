﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public static class Dapper
    {
        public static void Initialize(string mainConnectionString)
        {
            Repositories.Dapper.Initialize(mainConnectionString);
        }
    }
}
