﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api
{
    public static class UnitOfWork
    {
        private static Services.IUnitOfWorkFactory _unitOfWorkFactory;

        public static void Initialize(Services.IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public static Services.IUnitOfWork Start()
        {
            if (_unitOfWorkFactory == null)
            {
                throw new InvalidOperationException("UnitOfWork has not been initialized.");
            }

            return _unitOfWorkFactory.Create();
        }
    }
}