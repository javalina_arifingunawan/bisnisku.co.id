﻿using Api;
using System.Configuration;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(DapperConfig), "Register")]

namespace Api
{
    public class DapperConfig
    {
        public static void Register()
        {
            Services.Dapper.Initialize(ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
        }
    }
}