﻿using System.Web.Http;

namespace Api
{
    public class UnitOfWorkConfig
    {
        public static void Register(HttpConfiguration config)
        {
            UnitOfWork.Initialize((Services.IUnitOfWorkFactory)config.DependencyResolver.GetService(typeof(Services.IUnitOfWorkFactory)));
        }
    }
}