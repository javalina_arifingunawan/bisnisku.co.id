﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using Unity;
using Unity.WebApi;

namespace Api
{
    public static class UnityConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            var providers = config.Services.GetFilterProviders().ToList();
            var defaultprovider = providers.Single(p => p is ActionDescriptorFilterProvider);

            RegisterUnitOfWork(container);
            RegisterServices(container);
            RegisterRepositories(container);

            config.DependencyResolver = new UnityDependencyResolver(container);
            config.Services.Remove(typeof(IFilterProvider), defaultprovider);
            config.Services.Add(typeof(IFilterProvider), new UnityFilterProvider(container));
        }

        public static void RegisterUnitOfWork(IUnityContainer container)
        {
            container.RegisterType<Services.IUnitOfWorkFactory, Services.UnitOfWorkFactory>();
        }

        public static void RegisterServices(IUnityContainer container)
        {
            container.RegisterType<Services.ITaskFlow, Services.TaskFlow>();
        }

        public static void RegisterRepositories(IUnityContainer container)
        {
            container.RegisterType<Repositories.ITaskFlow, Repositories.TaskFlow>();
        }
    }
}