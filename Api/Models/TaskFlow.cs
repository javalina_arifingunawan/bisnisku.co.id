﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class TaskFlow
    {
        public Guid TaskFlowId { get; set; }
        public string TaskName { get; set; }
    }
}