﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    public class TaskFlowController : ApiController
    {
        Services.ITaskFlow _taskFlowService;

        public TaskFlowController(Services.ITaskFlow taskFlowService)
        {
            this._taskFlowService = taskFlowService;
        }

        public IHttpActionResult GetAllTaskFlow()
        {
            try
            {
                var result = _taskFlowService.GetAll();

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}
